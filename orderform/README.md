# Computer Components Order Form Project
This project provides a comprehensive example of a web-based order form for computer components. It demonstrates the use of HTML for form structure, CSS for styling, and JavaScript for dynamic interactions such as adding items to a shopping basket, generating an order ID, and downloading a PDF summary of the order.

## Project Structure
- `index.html`: The main HTML document that contains the structure of the order form.
- `css/styles.css`: The stylesheet that defines the look and feel of the order form.
- `js/orderFormScript.js`: Contains the logic for handling user interactions with the form, such as adding items to the basket, placing orders, and generating PDF summaries.

## Features
- **Dynamic Order Form**: Users can select computer components, configure their orders with additional options, and add them to a shopping basket.
- **Order Summary**: A detailed summary of the order, including selected components and configurations, is displayed in the basket.
- **PDF Summary Download**: Upon placing an order, a PDF summary of the order can be downloaded, which includes the order ID and details of the purchased items.
- **Responsive Design**: The form is styled with Bootstrap and custom CSS to ensure it is responsive and user-friendly across various devices and screen sizes.

## Setup
To run this project locally, clone the repository to your local machine. You can serve the project using any HTTP server, or simply open the `index.html` file in a web browser.

## Dependencies
- [Bootstrap](https://getbootstrap.com/): Used for styling the form and making it responsive.
- [jsPDF](https://github.com/parallax/jsPDF): A client-side JavaScript PDF generation library used for creating the order summary PDF.

## Usage
1. Open the `index.html` file in a web browser.
2. Fill in the customer information, select the operating system, model, and any additional configurations for your computer components.
3. Add items to the basket and review your selections.
4. Click the "Place Order" button to generate an order ID and download a PDF summary of your order.

## Live Demo
A live demo of the project is available at [https://jsfiddle.net/x1so5kjc/6/](https://jsfiddle.net/x1so5kjc/6/).