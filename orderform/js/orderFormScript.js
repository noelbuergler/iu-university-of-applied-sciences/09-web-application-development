/** @var {Array} basketItems - Holds the selected items for the basket */
var basketItems = [];

/**
 * Updates the model and configuration options based on the selected operating system.
 */
function updateModelOptions() {
    const osSelect = document.getElementById("osSelect");
    const selectedOS = osSelect.value;
    let modelHtml = '';
    let configHtml = '';

    if (selectedOS === "macOS") {
        modelHtml = `
            <option value="1100">MacBook Air - M2 - $1100</option>
            <option value="2300">MacBook Pro - M2 - $2300</option>
        `;

        configHtml = `
            <div class="form-group mb-3">
                <label for="extraSelect">Extras</label>
                <select class="form-control" id="extraSelect" multiple>
                    <option value="200">External Keyboard - $200</option>
                    <option value="150">External Mouse - $150</option>
                    <option value="300">Thunderbolt Dock - $300</option>
                </select>
                <small class="form-text text-muted">Hold <code>ctrl</code> on Windows or <code>cmd</code> to select multiple extras.</small>
            </div>
        `;
    } else if (selectedOS === "Windows") {
        modelHtml = `
            <option value="800">Lenovo ThinkPad X1 - $800</option>
            <option value="950">HP Spectre x360 - $950</option>
            <option value="1000">Dell XPS 15 - $1000</option>
        `;

        configHtml = `
            <div class="form-group mb-3">
                <label for="cpuSelect">CPU</label>
                <select class="form-control" id="cpuSelect">
                    <option value="0">Intel i5 - Included</option>
                    <option value="150">Intel i7 - $150</option>
                    <option value="250">Intel i9 - $250</option>
                </select>
            </div>
            <div class="form-group mb-3">
                <label for="gpuSelect">GPU</label>
                <select class="form-control" id="gpuSelect">
                    <option value="0">Integrated - Included</option>
                    <option value="200">NVIDIA GTX 1650 - $200</option>
                    <option value="400">NVIDIA RTX 3060 - $400</option>
                </select>
            </div>
            <div class="form-group mb-3">
                <label for="ramSelect">RAM</label>
                <select class="form-control" id="ramSelect">
                    <option value="0">16GB - Included</option>
                    <option value="100">32GB - $100</option>
                    <option value="200">64GB - $200</option>
                </select>
            </div>
        `;
    }

    // Update the HTML of the model select and configuration options
    document.getElementById("modelSelect").innerHTML = modelHtml;
    document.getElementById("configOptions").innerHTML = configHtml;
}

/**
 * Adds the selected notebook and configurations to the basket.
 */
function addToBasket() {
    // Retrieve customer information
    var customerName = document.getElementById("customerName").value;
    var customerEmail = document.getElementById("customerEmail").value;
    if (!customerName || !customerEmail.includes("@")) {
        alert("Please enter a valid name and email.");
        return;
    }

    // Logic to add selected notebook and configurations to the basket
    // Including parsing of prices to avoid NaN issues
    var modelSelect = document.getElementById("modelSelect");
    var selectedModel = modelSelect.options[modelSelect.selectedIndex];
    var price = parseFloat(selectedModel.value);
    var description = selectedModel.text;

    // Add selected configurations
    var configurations = [];
    document.querySelectorAll("#configOptions select").forEach(select => {
        var option = select.options[select.selectedIndex];
        configurations.push({ name: option.text, price: parseFloat(option.value) });
        price += parseFloat(option.value);
    });

    basketItems.push({ description, price, configurations });
    updateBasketUI();
}


/**
 * Updates the basket UI to reflect the current items.
 */
function updateBasketUI() {
    var basketList = document.getElementById("basketList");
    basketList.innerHTML = ""; // Reset basket list HTML

    var totalPrice = 0; // Variable to accumulate total price of all items

    // Iterate over basket items to display them
    basketItems.forEach((item, index) => {
        var itemElement = document.createElement("li");
        itemElement.innerHTML = `${item.description} - $${item.price.toFixed(2)}
            <button onclick="removeFromBasket(${index})">Remove</button>`;
        item.configurations.forEach(config => {
            itemElement.innerHTML += `<ul><li>${config.name} - $${config.price.toFixed(2)}</li></ul>`;
        });
        basketList.appendChild(itemElement);
        totalPrice += item.price;
    });

    // Display total price
    document.getElementById("totalPrice").textContent = `$${totalPrice.toFixed(2)}`;
}

/**
 * Removes an item from the basket based on its index.
 * @param {number} index - The index of the item in the basket to remove.
 */
function removeFromBasket(index) {
    basketItems.splice(index, 1); // Remove item from basket
    updateBasketUI(); // Update UI after item removal
}

/**
 * Validates the form fields before placing an order.
 */
function validateFormFields() {
    const customerName = document.getElementById("customerName").value;
    const customerEmail = document.getElementById("customerEmail").value;
    if (!customerName.trim() || !customerEmail.trim() || !customerEmail.includes("@")) {
        alert("Please fill out your name and a valid email address.");
        return false;
    }
    return true;
}

/**
 * Validates the form fields and then places the order, generating an order ID
 * and a downloadable PDF summary.
 */
function placeOrder() {
    // Check if basket is empty
    if (basketItems.length === 0) {
        alert("Your basket is empty.");
        return;
    }

    // Ensure customer information is filled out
    if (!validateFormFields()) {
        return; // Stop execution if validation fails
    }

    // Generate and display order ID
    const orderID = "ORDER-" + Math.floor(Math.random() * 1000000);
    document.getElementById("orderConfirmation").innerHTML = `Thank you for your order. Your order ID is ${orderID}. A PDF summary will be downloaded.`;

    // Download PDF summary
    downloadOrderSummary(orderID);

    // Reset basket items and update UI
    basketItems = [];
    updateBasketUI();
}

/**
 * Generates and downloads a PDF summary of the order.
 * @param {string} orderID - The generated order ID for the current transaction.
 */
function downloadOrderSummary(orderID) {
    const { jsPDF } = window.jspdf;
    const doc = new jsPDF();
    
    let content = `Order Summary:\nOrder ID: ${orderID}\n\n`;
    basketItems.forEach((item, index) => {
        content += `Item ${index + 1}: ${item.description}\n`;
        item.configurations.forEach(config => {
            content += `- ${config.name}: $${config.price.toFixed(2)}\n`;
        });
        content += `Total Price: $${item.price.toFixed(2)}\n\n`;
    });

    doc.text(content, 10, 10);
    doc.save('OrderSummary.pdf');
}

/**
 * Removes an individual component from an item in the basket.
 * @param {number} itemIndex - The index of the item from which the component will be removed.
 * @param {number} configIndex - The index of the component to remove.
 */
function removeConfig(itemIndex, configIndex) {
    basketItems[itemIndex].configDetails.splice(configIndex, 1); // Remove the specific configuration
    // Recalculate the total price for the item
    basketItems[itemIndex].totalPrice = calculateTotalPriceForItem(basketItems[itemIndex]);
    updateBasketUI();
}

/**
 * Calculates the total price for an item based on its configurations.
 * @param {Object} item - The item object containing configurations and base price.
 * @return {number} - The total price of the item.
 */
function calculateTotalPriceForItem(item) {
    let totalPrice = parseInt(item.basePrice);
    item.configDetails.forEach(config => {
        const pricePart = config.split('- $')[1];
        totalPrice += parseInt(pricePart ? pricePart : 0);
    });
    return totalPrice;
}