// Function to calculate the mean value of an array. The mean is the average value and is used in various parts of the linear regression calculation.
function mean(arr) {
    // The reduce method executes a reducer function for array element resulting in a single output value. The sum of all elements is divided by the array's length to find the mean.
    return arr.reduce((acc, val) => acc + val, 0) / arr.length;
}

// Function to calculate the covariance between x and y. Covariance indicates the direction of the linear relationship between variables.
function covariance(x, y) {
    // Calculate the mean of x and y using the mean function defined above.
    const xMean = mean(x);
    const yMean = mean(y);
    // The covariance is the sum of the product of each pair of values' deviations from their means, divided by the array length.
    return x.reduce((acc, val, i) => acc + (val - xMean) * (y[i] - yMean), 0) / x.length;
}

// Function to calculate the variance of x. Variance measures how far a set of numbers are spread out from their average value.
function variance(arr) {
    // Calculate the mean of the array.
    const arrMean = mean(arr);
    // The variance is the sum of the square of each value's deviation from the mean, divided by the array length.
    return arr.reduce((acc, val) => acc + Math.pow(val - arrMean, 2), 0) / arr.length;
}

// Function to calculate the coefficients b0 and b1 for the linear equation y = b0 + b1 * x.
function coefficients(x, y) {
    // b1 (the slope of the regression line) is the covariance of x and y divided by the variance of x.
    const b1 = covariance(x, y) / variance(x);
    // b0 (the intercept) is calculated by subtracting the product of b1 and the mean of x from the mean of y.
    const b0 = mean(y) - b1 * mean(x);
    // Return both coefficients as an array.
    return [b0, b1];
}

// Example data points representing x (independent variable) and y (dependent variable).
const x = [1, 2, 3, 4, 5];
const y = [2, 3, 5, 7, 11];

// Calculate coefficients using the coefficients function.
const [b0, b1] = coefficients(x, y);

// Log the linear equation to the console. This shows the calculated line of best fit.
console.log(`Linear equation: y = ${b0.toFixed(2)} + ${b1.toFixed(2)}x`);

// Function to predict the value of y given a new x using the linear equation y = b0 + b1 * x.
function predict(x) {
    // Apply the linear equation to the new x value.
    return b0 + b1 * x;
}

// Predict the value of y for a new x value (6 in this case) and log the result to the console.
const xToPredict = 6;
console.log(`Predicted value at x = ${xToPredict}: y = ${predict(xToPredict).toFixed(2)}`);